#pragma once

#define APP_NAME "MineCraft Clone"
#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768

#define FRAME_PER_SECOND 60.0f
#define SECOND_PER_FRAME 1.0f/ FRAME_PER_SECOND