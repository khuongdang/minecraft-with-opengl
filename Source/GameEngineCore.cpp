#include "GameEngineCore.h"

namespace GameEngineCore {
	GECore::GECore()
	{
	}
	GECore::~GECore()
	{
	}

	GECore * GECore::Init()
	{
		if (m_Instance == nullptr)
			m_Instance = new GECore;

		//INIT
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); //For Mac user not crash
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		//Create window
		m_Instance->m_window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, APP_NAME, nullptr, nullptr);

		int screenWidth, screenHeight;
		glfwGetFramebufferSize(m_Instance->m_window, &screenWidth, &screenHeight);

		if (m_Instance->m_window == nullptr)
		{
			std::cout << "Failed to create GLFW window" << std::endl;
			glfwTerminate();

			return nullptr;
		}
		glfwMakeContextCurrent(m_Instance->m_window);

		// tell GL to use the modern way
		glewExperimental = GL_TRUE;

		if (glewInit() != GLEW_OK)
		{
			std::cout << "Failed to initialize GLEW" << std::endl;

			return nullptr;
		}

		glViewport(0, 0, screenWidth, screenHeight);

		//Create Gamemanager

		GameManager::Init();

		return m_Instance;
	}

	GLuint GECore::Loop()
	{

		glfwPollEvents();

		GameManager::GetInstance()->Loop();

		return !m_GameShouldEnd();
	}

	GLuint GECore::End()
	{
		glfwTerminate();

		return EXIT_SUCCESS;
	}

	GECore * GECore::Get_Instance()
	{
		return m_Instance;
	}

	GLFWwindow * GECore::Get_Window()
	{
		return GECore::Get_Instance()->m_window;
	}

	GLboolean GECore::m_GameShouldEnd()
	{
		return glfwWindowShouldClose(m_window);
	}

	GECore * GECore::m_Instance = nullptr;
}
