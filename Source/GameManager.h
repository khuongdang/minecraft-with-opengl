#pragma once

#include <gl/glew.h>
#include <chrono>
#include "Renderer.h"
#include "Definition.h"

class GameManager
{
public:
	static GameManager* Init();
	static GameManager* GetInstance();
	GLboolean Loop();

private:
	enum State
	{
		MENU,
		INGAME,
		PAUSE,
		GAMEOVER
	}; 

	std::chrono::time_point<std::chrono::system_clock> LastFrame = std::chrono::system_clock::now();

	static GameManager * m_Instance;

	Renderer m_Renderer;

	GameManager();
	~GameManager();

public:
	GameManager(GameManager &const) = delete;
	void operator=(GameManager &const) = delete;

};

