#pragma once

#include <iostream>

#define GLEW_STATIC

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include "Definition.h"

#include "GameManager.h"

namespace GameEngineCore {
	class GECore
	{
		public:
			static GECore* Init();
			static GECore* Get_Instance();
			static GLFWwindow* Get_Window();
			
			static GLuint  End();

			GLuint  Loop();

		private:
			GECore();
			~GECore();

			static GECore * m_Instance;
			GLboolean m_GameShouldEnd();
			GLFWwindow* m_window;
	};
}