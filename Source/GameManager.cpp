#include "GameManager.h"
#include "GameEngineCore.h"

static const GLfloat g_vertex_buffer_data[] = {
	-0.5f, -0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	0.0f,  0.5f, 0.0f,
};

GLuint vertexbuffer;
GLuint VertexArrayID;

GameManager * GameManager::Init()
{
	
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// This will identify our vertex buffer

	// Generate 1 buffer, put the resulting identifier in vertexbuffer
	glGenBuffers(1, &vertexbuffer);
	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	if (m_Instance == nullptr)
		m_Instance = new GameManager();

	m_Instance->m_Renderer = Renderer();

	return m_Instance;
}

GameManager * GameManager::GetInstance()
{
	return m_Instance;
}

GLboolean GameManager::Loop()
{
	//update here

	//////

	std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
	std::chrono::duration<double> From_lastframe = now - LastFrame;
	if (From_lastframe.count() >= SECOND_PER_FRAME)
	{
		LastFrame = std::chrono::system_clock::now();

		//glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		//glClear(GL_COLOR_BUFFER_BIT);

		//m_Renderer.Render();
		// 1st attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);
		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, 3); // Starting from vertex 0; 3 vertices total -> 1 triangle
		glDisableVertexAttribArray(0);

		glfwSwapBuffers(GameEngineCore::GECore::Get_Window());
	}

	return true;
}

GameManager::GameManager()
{
}


GameManager::~GameManager()
{
	delete m_Instance;
}

GameManager * GameManager::m_Instance = nullptr;
