#pragma once
#include "glm.hpp"

class Object {
private:
	glm::vec3 m_position = glm::vec3(0.0f);
public:
	virtual void Render() = 0;
};