#pragma once
#include "Object.h"
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "glm.hpp"

class Block :
	public Object
{
private:
	glm::vec3 m_size = glm::vec3(1.0f);

public:
	Block();
	~Block();
};

